package org.tu_dresden.robosimulator;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class Figur {
    private  Vektor position;
    private String bezeichnung;
    private Color farbe;

    public Figur(Vektor position, String bezeichnung, Color farbe) {
            this.position=position;
            this.bezeichnung = bezeichnung;
            this.farbe = farbe;

        }

    public void setPosition(Vektor position) {
        this.position = position;
    }

    public Vektor getPosition() {
        return position;
    }

    public void setFarbe(Color farbe) {
        if(farbe.equals(Color.white)){
            System.err.println("Rechteck darf nicht weiß sein");
            return;
        }

        this.farbe = farbe;
    }

    public void bewege_um(double dx,double dy){
        position.setX(position.getX()+dx);
        position.setY(position.getY()+dy);
    }


    public void bewege_um(Vektor vektor){
        bewege_um(vektor.getX(),vektor.getY());
    }

    public Color getFarbe() {
        return farbe;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }
    public abstract double minX();
    public abstract double maxX();
    public abstract double minY();
    public abstract double maxY();

    public boolean überlappt(Figur f){
        return  maxX()>=f.minX()&&minX()<=f.maxX()
                &&  maxY()>=f.minY()&&minY()<=f.maxY();
    }
    public boolean punkt_innerhalb(Vektor v){
        boolean resX = minX()<=v.getX() &&v.getX()<=maxX();
        boolean resY = minY()<=v.getY()&&v.getY()<=maxY();
        boolean res =  resX&&resY;
        if(res){
            return true;
        }else return false;
    }

    /** Gibt die Figuren zurück welche mit der Figur überlappen
     * @param figuren Die Figuren die getesetet werden soll
     * @param <T> Die Klasse der Figuren (der Typ z.B. Rechteck)
     * @return Die Lise der Objekte aus "figuren" welche mit der Figure überlappt
     */
    public <T extends Figur> ArrayList<T> überlapptFigur(T[] figuren){
        return (ArrayList<T>) überlappt(new ArrayList<Figur>((ArrayList<T>)Arrays.asList(figuren)));
    }
    /** Gibt die Figuren zurück welche mit der Figur überlappen
     * @param figuren Die Figuren die getesetet werden soll
     * @return Die Lise der Objekte aus "figuren" welche mit der Figure überlappt
     */
    public ArrayList<? extends Figur> überlappt(ArrayList<? extends Figur> figuren){
        ArrayList<? extends Figur> res = (ArrayList<? extends Figur>) figuren.clone();
        for(Figur f:figuren){
            if(!überlappt(f))
                res.remove(f);
        }
        return res;
    }

    /** Zeichnet die Figur auf ein Java2D Grafik Objekt
     * @param g Das Grafik Objekt
     */
    public abstract void zeichnen(Graphics2D g);



}

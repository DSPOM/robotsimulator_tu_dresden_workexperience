package org.tu_dresden.robosimulator.test;

import org.tu_dresden.robosimulator.Rechteck;
import org.tu_dresden.robosimulator.Vektor;

import java.awt.*;

public class Uebung2 {
    public static void main(String[] args){
        Rechteck r = new Rechteck(new Vektor(24,42),4,2, "Test",Color.ORANGE);
        r.print();
        r.bewege_um(2,4);
        r.setBreite(42);
        r.setBezeichnung("Test2");
        r.print();

    }
}

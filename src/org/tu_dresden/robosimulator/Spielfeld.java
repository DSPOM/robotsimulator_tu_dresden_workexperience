package org.tu_dresden.robosimulator;

import java.awt.*;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Spielfeld {

    private static Spielfeld spielfeld;
    public static Spielfeld getInstance(){
        return spielfeld==null?spielfeld=new Spielfeld():spielfeld;
    }

    public final int SpielfieldBreite = 1000;
    public final int SpielfieldHoehe = 1000;
    public final int MaxHindernissHöhe = 100;
    public final int MaxHindernissBreite = 100;

    private Leinwand leinwand;
    private Roboter roboter;
    private static Random zufallsgenerator = new Random();

    private Spielfeld(){
        leinwand = new Leinwand(1000,1000,Color.WHITE);
        roboter = new Roboter("Wall-E","BNL",10, leinwand);
    }



    private static int ganzeZahlEingeben(Scanner input){
        int res;
        while (true){
            try {
                res=input.nextInt();
                return res;
            }catch (InputMismatchException e){
                System.err.println("Bitte geben sie einge ganze Zahl ein");
            }
        }
    }

    public Vektor[] punkteEingeben(){
        System.out.print("Punktanzahl eingeben: ");
        Scanner input = new Scanner(System.in);
        Vektor[] res = new Vektor[ganzeZahlEingeben(input)];
        int x_in;
        int y_in;
        int i;
        for(i=0;i<res.length;i++){
            System.out.print("\n");
            System.out.print("Neuer Punkt mit X: ");
            x_in=ganzeZahlEingeben(input);

            while (x_in<0||x_in>SpielfieldBreite){
                System.err.print("Koordinate muss im Spielfeld liegen. Bitte erneut eingeben: ");
                x_in=ganzeZahlEingeben(input);
            }
            System.out.print(", Y: ");
            y_in=ganzeZahlEingeben(input);
            while (y_in<0||y_in>SpielfieldHoehe){
                System.err.print("Koordinate muss im Spielfeld liegen. Bitte erneut eingeben: ");
                y_in=ganzeZahlEingeben(input);
            }
            res[i]=new Vektor(x_in,y_in);
        }
        System.out.println("Soritere Punkte:");
        res = POI_sortieren(res);
        for(i=0;i<res.length;i++){
            res[i].aktuellePositionAusgeben();
            if(i<res.length-1)
            System.out.println("Abstand: "+res[i].gibAbstand(res[i+1]));
        }
        return res;

    }

    public Vektor[] POI_sortieren( Vektor[] poi) {

        Vektor[] res = new Vektor[poi.length];
        res[0] = poi[0];
        poi[0] = null;
        double min_distance;
        int closest_point_index = 0;
        double currentdistance;

        for  (int i = 0;i<res.length-1;i++) {
            min_distance = Integer.MAX_VALUE;
            for(int j = 0;j< poi.length;j++){
                if(poi[j]==null)continue;
                currentdistance = res[i].gibAbstand(poi[j]);
                if(currentdistance<min_distance){
                    min_distance=currentdistance;
                    closest_point_index=j;
                }
            }
            res[i+1] = poi[closest_point_index];
            poi[closest_point_index]=null;

        }

        return res;
    }
    /*
       Fügt ein Hinderniss hinzu, falls es nicht mit einem existierndem Hinderniss überlappt
       @param hindernisse Liste von Hindernissen zu denen hinderniss hinzugefügt werden soll
        @param hinderniss hinzuzufügendes Hiderniss
        @return Ob das Hinderniss hinzugefügt werden konnte
     */
    public boolean add_Hinderniss(ArrayList<Rechteck> hindernisse, Rechteck hinderniss) {
        for (Rechteck i : hindernisse)
            if (i.überlappt(hinderniss)) {
                System.err.println("Hinderniss" + hinderniss + " überlappt mit vorhandenen Hinderniss " + i);
                return false;
            }
        if(roboter.überlappt(hinderniss))return false;
        return hindernisse.add(hinderniss);
    }

    public ArrayList<Rechteck> hindernisliste_erzeugen(){
        Scanner input = new Scanner(System.in);
        System.out.print("Anzahl der zu erzeugenden Hindernisse eingeben: ");
        int anzahl = ganzeZahlEingeben(input);
        return hindernisliste_erzeugen(anzahl);
    }
    public ArrayList<Rechteck> hindernisliste_erzeugen(int anzahl){

        ArrayList<Rechteck> res = new ArrayList<>(anzahl);

        int trys;
            for (int i = 0; i < anzahl; i++) {
                trys = 0;
                while (!add_Hinderniss(res,hiderniss_erzeugen("Rechteck " + i))) {
                    if (trys++ >= 50) {
                        throw new RuntimeException("Das Spielfeld ist zu klein um sinvoll mit " + anzahl + " Hidernissen gefüllt zu werden!");
                    }
                }
            }
            return res;
    }

    /*
        Erzeugt ein zufällige ganze Zahl x in einem Begrenzten Intervall
        @param von minimal mögliche Zahl x >= von
        @param bis Oberegrenze der Zufallszahl (exclusiv) x < bis

    */
    private static int zufallszahl(int von,int bis){
        if(bis<von)throw new IllegalArgumentException("Die Grenze der Zufallszahlen darf nicht kleiner als die minimale Zufallszahl sein! Gegeben: "+von+"-"+bis);
        return zufallsgenerator.nextInt(bis-von)+von;
    }

    /*
        Erzeugt ein zufällige Farbe, die nicht absolut weiß ist
    */
    private static Color zufallsfarbe_nichtweiß(){
        return new Color(zufallszahl(0,(int) Math.pow(2,24)-1)); //2^24(-1) sind alle möglichen RGB Werte (3x8 bit in einerzahl) und -1 damit kein weißes rechteck generiert wird
    }

    /*
        Erzeugt ein zufällige Farbe
    */
    private static Color zufallsfarbe(){
        return new Color(zufallszahl(0,2^24));
    }

    /*
        Erzeugt ein zufälligs Hiderniss innerhalb des Spielfedes
        @param bezeichnung Bezecihnung des Spielfedes
        @return Das erzeugte Rechteck
    */
    private Rechteck hiderniss_erzeugen(String bezeichnung){
        int x,y,breite,länge;
        breite = zufallszahl(1, MaxHindernissBreite+1);
            länge = zufallszahl(1, MaxHindernissHöhe+1);
            x = zufallszahl(0, SpielfieldBreite-breite);
            y = zufallszahl(0, SpielfieldBreite-länge);

        return new Rechteck(new Vektor(x,y),breite,länge,bezeichnung,zufallsfarbe_nichtweiß());
    }


    public void zeichnen(ArrayList<? extends Figur> hindernisse,boolean roboter){
        ArrayList<Figur> res;
        if(hindernisse != null)
            res = new ArrayList<>(hindernisse);
        else
            res = new ArrayList<>();
        if(roboter){
            res.add(this.roboter);
        }
        leinwand.zeichnen(res);
    }




    public static void main(String[] args){
        String command;
        Scanner input = new Scanner(System.in);
        System.out.println("Bitte eines der Kommandos: POI, Hindernisse, Spracherkennung oder Ende eingeben!");
        while (!(command=input.nextLine()).equals("Ende")){
            switch (command.toLowerCase()) {
                case "poi":
                    getInstance().roboter.setPosition(new Vektor(getInstance().roboter.getRadius(), getInstance().roboter.getRadius()));
                    getInstance().zeichnen(null,true);
                    getInstance().roboter.poi_anfahren(getInstance().punkteEingeben());
                    Leinwand.warten(5000);
                    break;

                case "hindernisse":

                        getInstance().roboter.setPosition(new Vektor(getInstance().roboter.getRadius(), getInstance().roboter.getRadius()));
                        ArrayList<Rechteck> hindernisse;
                        hindernisse = getInstance().hindernisliste_erzeugen();
                        hindernisse.add(new Rechteck(new Vektor(-1, -6), getInstance().SpielfieldBreite, 5, "Wand_Oben", Color.BLACK));
                        hindernisse.add(new Rechteck(new Vektor(-6, -1), 5, getInstance().SpielfieldHoehe, "Wand_Links", Color.BLACK));
                        hindernisse.add(new Rechteck(new Vektor(getInstance().SpielfieldBreite + 1, -1), 5, getInstance().SpielfieldHoehe + 1, "Wand_Rechts", Color.BLACK));
                        hindernisse.add(new Rechteck(new Vektor(-1, 1 + getInstance().SpielfieldHoehe), getInstance().SpielfieldBreite + 1, 5, "Wand_Unten", Color.BLACK));
                        getInstance().zeichnen(hindernisse,true);
                        if (getInstance().roboter.hindernisse_umfahren(hindernisse, new Vektor(getInstance().SpielfieldBreite - getInstance().roboter.getRadius(), getInstance().SpielfieldHoehe - getInstance().roboter.getRadius())))
                            System.out.println("YAY!");
                        else
                            System.out.println("Ziel konnte nicht erreicht werden");
                        break;

                case "spracherkennung":
                    getInstance().roboter.spracherkennung();
                    break;
            }
            System.out.println("Bitte eines der Kommandos: POI, Hindernisse, Spracherkennung oder Ende eingeben!");
        }
    }

}

package org.tu_dresden.robosimulator;

import java.awt.*;

public class Rechteck extends Figur{


    private int breite, länge;


    public Rechteck(Vektor position, int breite, int länge, String bezeichnung, Color farbe ) {
        super(position,bezeichnung,farbe);
        this.breite = breite;
        this.länge = länge;

    }


    //getters & setters


        public int getBreite() {
            return breite;
        }

        public void setBreite(int breite) {
            this.breite = breite;
        }

        public int getLänge() {
            return länge;
        }

        public void setLänge(int länge) {
            this.länge = länge;
        }
    public void print(){
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "Rechteck "+getBezeichnung()+" an "+getPosition().toString()+" Abemssungen: ("+getLänge()+":"+getBreite()+") Farbe: "+getFarbe().getRGB();
    }


    @Override
    public double minX() {
        return getPosition().getX();
    }

    @Override
    public double maxX() {
        return getPosition().getX()+ getBreite();
    }

    @Override
    public double minY() {
        return getPosition().getY();
    }

    @Override
    public double maxY() {
        return getPosition().getY()+ getLänge();
    }

    @Override
    public void zeichnen(Graphics2D g) {
        g.setColor(getFarbe());
        g.fillRect((int)Math.round(minX()),(int)Math.round(minY()),getBreite(),getLänge());
    }
}

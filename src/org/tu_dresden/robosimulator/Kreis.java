package org.tu_dresden.robosimulator;

import java.awt.*;

public class Kreis extends Figur {

    private double radius;
    public Kreis(Vektor position, String bezeichnung, Color farbe,double radius) {
        super(position, bezeichnung, farbe);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double minX() {
        return getPosition().getX()-radius;
    }

    @Override
    public double maxX() {
        return getPosition().getX()+radius;
    }

    @Override
    public double minY() {
        return getPosition().getY()-radius;
    }

    @Override
    public double maxY() {
        return getPosition().getY()+radius;
    }

    @Override
    protected Kreis clone()  {
        return new Kreis(getPosition().clone(),new String(getBezeichnung()),getFarbe(),getRadius());
    }

    @Override
    public boolean punkt_innerhalb(Vektor v) {
        return getPosition().gibAbstand(v)<=getRadius();
    }

    @Override
    public void zeichnen(Graphics2D g) {
        g.setColor(getFarbe());
        g.fillOval((int)Math.round(minX()),(int)Math.round(minY()),(int)getRadius()*2,(int)getRadius()*2);
    }

    /**
     * Gibt an ob eine Linie diesen Kreis schneidet
     * @param endpunkt1 Das 1. Ende der Linie
     * @param endpunkt2 Das 2. Ende der Linie
     * @return Ob die Linie den Kreis schneidet
     */
    public boolean schneidet_linie(Vektor endpunkt1,Vektor endpunkt2){
        boolean res;
        Vektor verbindungsvektor = endpunkt1.sub(endpunkt2);
        Vektor Vektor_zu_Kreis = getPosition().sub(endpunkt2);
        double projektion =  Vektor_zu_Kreis.skalarprodukt(verbindungsvektor)/verbindungsvektor.getLänge();
        if(projektion<0||projektion>verbindungsvektor.getLänge()) {
            res = endpunkt1.gibAbstand(getPosition()) <= getRadius() || endpunkt2.gibAbstand(getPosition()) <= getRadius();
            if(res){
                return true;
            }else return false;
        }
        verbindungsvektor.setLänge(projektion);
        res =  punkt_innerhalb(endpunkt2.add(verbindungsvektor));
        if(res){
            return true;
        }else return false;
    }

    /**
     * Ob ein Figur (bis jetzt nur für Kreis oder rechteck implementiert) mit diesem Kreis überlappt
     * @param f Die Figur die überprüft
     * @return Ob die Figur mit diesem Kreis überlappt
     */
    @Override
    public boolean überlappt(Figur f){
        if(f instanceof Kreis)
            return f.getPosition().sub(getPosition()).getLänge()<=getRadius()+((Kreis)f).getRadius();
        else if(f instanceof Rechteck) {
            Vektor ecke1 = f.getPosition();
            Vektor ecke2 = ecke1.clone();
            ecke2.setX(f.maxX());
            Vektor ecke3 = ecke1.clone();
            ecke3.setY(f.maxY());
            Vektor ecke4 = new Vektor(f.maxX(), f.maxY());
            boolean res = f.punkt_innerhalb(getPosition())
                    || schneidet_linie(ecke1, ecke2)
                    || schneidet_linie(ecke2, ecke4)
                    || schneidet_linie(ecke1, ecke3)
                    || schneidet_linie(ecke3, ecke4);
            if (res) {
                return true;
            } else return false;
        }else
            return f.überlappt(this);
    }
}

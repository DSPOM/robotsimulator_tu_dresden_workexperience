package org.tu_dresden.robosimulator;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Leinwand {
    private JFrame fenster;
    private Zeichenflaeche zeichenflaeche;
    private int breite;
    private int länge;
    private Color hintegrundFarbe;
    public Leinwand(int breite,int länge,Color hintegrundFarbe){
        this.länge=länge;
        this.breite=breite;
        this.hintegrundFarbe=hintegrundFarbe;
        zeichenflaeche = new Zeichenflaeche();
        fenster = new JFrame();
        fenster.setSize(breite,länge);
        fenster.setBackground(hintegrundFarbe);
        fenster.setContentPane(zeichenflaeche);
        fenster.setVisible(true);
        fenster.setSize(breite,länge);
    }
    private class Zeichenflaeche extends JPanel{
        ArrayList<Figur> figuren = new ArrayList<>();
        Kreis roboter;

        @Override
        protected void paintComponent(Graphics g) {
            g.setColor(hintegrundFarbe);
            g.fillRect(0,0,breite,länge);
            if(figuren!=null)
            for(Figur f: figuren){
                f.zeichnen((Graphics2D)g);
            }
        }
    }

    public void zeichnen(ArrayList<Figur> figuren){
        zeichenflaeche.figuren = figuren;
        fenster.repaint();
    }

    /**
     * Animert die Bewegung einer Figur zu einem Ziel mit angegebener geschwindigkeit
     * @param f Die animierte Figur
     * @param ziel Das Ziel
     * @param speed Die Geschwindigkeit mit der sich die Figur bewegen soll (in px/sec)
     */
    public void animieren(Figur f, Vektor ziel,double speed){
        Vektor bewgungsVektor = ziel.sub(f.getPosition());
        int reisedauer= (int) (bewgungsVektor.getLänge());
        bewgungsVektor.setLänge(1);
        startAnimation();
        for (int reiseZeit = 0;reiseZeit<reisedauer;reiseZeit++){
            f.bewege_um(bewgungsVektor);
            frame((int)(1000/speed));
        }
    }

    /**
     * Muss vorm start einer animation augferufen werden
     */
    public void startAnimation(){
        lastframe=System.currentTimeMillis();
    }
    private long lastframe=System.currentTimeMillis();

    /**
     * Muss während einer animation immer aufgerufen werden wenn ein neues Frame generiert wird.
     * @param frametime Die Zeit die Seit dem letzten frame (mindestens) vergangen sein soll
     */
    public void frame(long frametime){
        warten(frametime-System.currentTimeMillis()+lastframe);
        fenster.repaint();
        Toolkit.getDefaultToolkit().sync();
        lastframe = System.currentTimeMillis();
    }

    /** Wartet eine bestimmte Anzahl an Millisekunden. Falls diese negativ oder 0 sind wird nicht gewartet
     * @param milli Anzahl der Millisekunden der gewartet werden soll.
     */
    public static void warten(long milli){
        if(milli<=0)return;
        try {
            Thread.sleep(milli);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
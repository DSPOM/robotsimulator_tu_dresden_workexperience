package org.tu_dresden.robosimulator;


/*
Punkte werden auch als Vektoren behandelt. Da aber die verwendung im Programm definitiv öfter den Funktionen eines Vektors entspricht werden Punkt P einfach als Vektoren 0P behandelt. (implimentiert die Funktunalität der Punktklasse aus der Aufgabenstellung vollständig)
 */
public class Vektor {

    private double x, y;


    public Vektor(){}

    public Vektor(double x,double y){
        this.x=x;
        this.y=y;
    }


    //getters & setters
        public double getX(){
            return x;
        }
        public void setX(double p_x){
            x=p_x;
        }
        public void setY(double p_y){
            y=p_y;
        }
        public double getY(){
            return y;
        }
        public void aktuellePositionAusgeben(){
            System.out.println(toString());
        }

    @Override
    public String toString() {
        return "("+x+","+y+")";
    }

    public Vektor sub( Vektor p){
        return sub(p.getX(),p.getY());
    }
    public Vektor sub(double x, double y){
        return new Vektor(getX()-x,getY()-y);
    }
    public Vektor add(Vektor p){
        return new Vektor(getX()+p.getX(),getY()+p.getY());
    }
    public double getLänge(){
        return Math.sqrt(Math.pow(getX(),2)+Math.pow(getY(),2));
    }
    public double gibAbstand(Vektor to){
            return sub(to).getLänge();
        }
    public void setLänge(double neu_länge){
        double länge = getLänge();
        setX(neu_länge/länge*getX());
        setY(neu_länge/länge*getY());
    }
    public double skalarprodukt(Vektor p){
        return p.getX()*getX()+p.getY()*getY();
    }
    public Vektor clone(double neu_länge){
        Vektor res = clone();
        res.setLänge(neu_länge);
        return res;
    }
    @Override
    public Vektor clone() {
        return new Vektor(getX(),getY());
    }
}

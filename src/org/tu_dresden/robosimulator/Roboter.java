package org.tu_dresden.robosimulator;


import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;

public class Roboter extends Kreis{
    private String name;
    private String hersteller;
    public static final double defaultspeed = 120;
    public static final Color robotcolor = Color.RED;
    private Leinwand leinwand;

    private double speed; //Speed in px/sec

    public Roboter(String name,String hersteller,int radius,Leinwand leinwand){
        this(name,hersteller,new Vektor(0,0),defaultspeed,radius,leinwand);
    }
    public Roboter(String name,String hersteller,Vektor position,double speed,int radius,Leinwand leinwand){
        super(position,name, robotcolor,radius);
        this.name=name;
        this.hersteller=hersteller;
        this.speed=speed;
        this.leinwand = leinwand;
    }

    public String getHersteller() {
        return hersteller;
    }

    public void setHersteller(String hersteller) {
        this.hersteller = hersteller;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    enum Stichwort{
        NAME,HERSTELLER,SINN,DATUM;
        @Override
        public String toString() {
            return this.name();
        }
    }

    /**
     *  User can Stichwörter solange eingeben bis er Ende eingibt und der Roboter reagiert auf diese
     */
    public void spracherkennung(){
        Scanner input = new Scanner(System.in);
        String text;
        String[] words;
        ArrayList<Stichwort> keywords;
        Stichwort keyword;
        while(!(text=input.nextLine()).equals("Ende")){ //solange spracherkennung durchühren bis der Benutzer Ende eingibt.
            //Eingabe in einzelne Wörter unterteilen
            keywords = new ArrayList<>(4);
            words = text.split(" ");
            //Stichworte erkennen
            for(String word:words){
                try {
                    keywords.add(keyword=Stichwort.valueOf(word.toUpperCase()));
                }catch (IllegalArgumentException e){
                    //not a keyword
                }
            }
            //Je nach Anzahl der Stichworte auf diese reagieren
            switch (keywords.size()){

                case 0: System.err.println("Ich weiß nicht was ich dazu sagen soll. Meine Sprachkentnisse sind leider noch sehr beschränkt. Bitte verwende mindestens eins der folgenden Stichwörter: "+ Arrays.toString(Stichwort.values()));
                    break;
                case 1:
                    switch (keywords.get(0)){
                        case NAME:

                            //falls die Indikatoren einer frage nicht gegeben sind kontrollieren ob es sich um eine Aussage handelt und auf diese eingehen
                            if(!words[0].startsWith("W")&&!words[words.length-1].endsWith("?")) {
                                for (int i = 0;i<words.length;i++) {
                                    if (words[i].equals("ist")||words[i].equals("heiße")){
                                        System.out.println("Hallo "+words[i+1]+"! ");
                                    }
                                }
                            }

                            //Frage beantworten bzw. Im Fall einer aussage wie erwartet den eigenen Name ausgeben.
                            System.out.println("Ich heiße "+getName()+"\n");
                            break;
                        case HERSTELLER:

                            //falls die Indikatoren einer frage nicht gegeben sind kontrollieren ob es sich um eine Aussage handelt und auf diese eingehen
                            if(!words[0].startsWith("W")&&!words[words.length-1].endsWith("?")) {
                                for (int i = 0;i<words.length;i++) {
                                    if (words[i].equals("ist")){
                                        System.out.println("Es ist merkwürdig, dass du deine Eltern als Hersteller bezeichnest!");
                                        break; //hier macht es weniger Sinn nachdem auf die Aussage reagiert wurde noch den eigenen hersteller anzugeben.
                                    }
                                }
                            }

                            //Frage beantworten
                            System.out.println("Ich bin eine Simulation aber mein echtes Vorbild wird von "+getHersteller()+" entwickelt");
                            break;

                        case SINN:
                            //falls die Indikatoren einer frage nicht gegeben sind kontrollieren ob es sich um eine Aussage handelt und auf diese eingehen
                            if(!words[0].startsWith("W")&&!words[words.length-1].endsWith("?")&&!words[0].equalsIgnoreCase("Macht")) {
                                for (int i = 0;i<words.length;i++) {
                                    if (words[i].equals("macht")||words[i].equals("hat")){
                                        for(int j = 0;j<words.length;i++){
                                            if(words[j].equals("keinen")||words[j].equals("nicht")&&words[j+1].equals("viel")){
                                                System.out.println("Tut mir leid. Ich bin keine KI; Ich bin nicht :/");
                                            }
                                        }
                                        System.out.println("YAY!");
                                        break; //hier macht es weniger Sinn nachdem auf die Aussage reagiert wurde noch den eigenen hersteller anzugeben.
                                    }
                                }
                            }

                            //Frage beantworten
                            if(words[0].startsWith("W"))
                                System.out.println("42");
                            else if(words[0].equals("Kannst")){
                                System.out.println("Nein");
                            }else{
                                System.out.println("Weiß ich auch nicht.");
                            }
                            break;
                        case DATUM:
                            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                            Date date = new Date();
                            System.out.println(dateFormat.format(date));
                    }
                    break;

                default: System.err.println(""+keywords.size()+" Stichwörter sind noch nicht unterstützt :/");
                break;
            }
        }
    }

    public void poi_anfahren( Vektor[] poi){
        setPosition(poi[0]);
        leinwand.frame((int)(1000/speed));
        //Jeder durchlauf ist bewegung von poi[i] nach poi[i+1]
        for(int i = 1;i<poi.length;i++){
            fahren(poi[i]);
        }
    }

    /**
     * Der Robotr fährt zu einem Ziel
     * @param ziel
     */
    public void fahren( Vektor ziel){
        leinwand.animieren(this,ziel,speed);
        setPosition(ziel);
        leinwand.frame((int)(1000/speed));
    }
    /**
     * Der Roboter fährt zum Ziel. Falls er dabei mit einem Hinderniss kollidiert bricht die Methode ab und gibt diese zurück.
     * @param ziel Das Ziel zu dem der Roboter proibieren soll zu fahren
     * @param hindernisse Die Hindernisse die potenzeill den Weg versperren könnten
     * @return Das Hindernisse mit dem Roboter kollidiert nicht. null bei erfolg.
     */
    public <T extends Figur> ArrayList<T> fahren(Vektor ziel,ArrayList<T> hindernisse) {
        return fahren(ziel,hindernisse,false);
    }
        public <T extends Figur> ArrayList<T> fahren(Vektor ziel,ArrayList<T> hindernisse,boolean startAnimation){
        Vektor bewgungsVektor = ziel.sub(getPosition());
        Kreis simuliertesZiel = super.clone();
        ArrayList<T> kollisionen;
        int reisedauer= (int) (bewgungsVektor.getLänge());
        bewgungsVektor.setLänge(1);
        if(startAnimation)        leinwand.startAnimation();

        for (int reiseZeit = 0;reiseZeit<reisedauer;reiseZeit++){
            simuliertesZiel.bewege_um(bewgungsVektor);
            kollisionen = (ArrayList<T>) simuliertesZiel.überlappt(hindernisse);
            if(!kollisionen.isEmpty())
                return kollisionen;
            bewege_um(bewgungsVektor);
            leinwand.frame((long)(1000/speed));
        }
        setPosition(ziel);
        leinwand.frame((long)(1000/speed));
        return null;
    }


    /**
     * Umfährt ein Hinderniss. Es wird angenommen, dass der Robot direkt an diesem hinderniss dran ist.
     * Falls der Roboter dabei auf ein anderes Hinderniss trifft, schlägt die Methode fehll.
     * @param hinderniss Das zu umfahrende Hinderniss
     * @param kontext Andere HIndernisse die den Weg versperren könnten
     * @return Ob das hinderniss erfolgreich umfahren wurde
     */
    private boolean hinderniss_umfahren(Rechteck hinderniss,ArrayList<Rechteck> kontext) {
        ArrayList<Rechteck> problem = an_untere_wand_fahren(hinderniss, kontext);
        if (problem == null) return true;
        if(problem.size()>1)return false;
        Rechteck problem1 = problem.get(0);
        problem = an_rechte_wand_fahren(hinderniss, kontext);
        if (problem == null) return true;
        if(problem.size()>1)return false;
        Rechteck problem2 = problem.get(0);
        if (problem1 == hinderniss && problem2 == hinderniss) {
            problem = an_linke_wand_fahren(hinderniss, kontext);
            if (problem != null) return false;
            hinderniss_umfahren(hinderniss, kontext);
        }
        return false;
    }

    /**
     * Fährt nach einen Ziel und muss dabei Hindernissen ausweichen
     * @param hindernisse Die Hindernisse die
     * @param ziel
     * @return
     */
    public boolean hindernisse_umfahren(ArrayList<Rechteck> hindernisse,Vektor ziel){
        Vektor richtung = ziel.sub(getPosition());
        if(richtung.getX()<0||richtung.getY()<0){
            throw new IllegalArgumentException("Hindernisse können nur in positive Richtung umfahren werden!");
        }
        ArrayList<Rechteck> kollisionen = fahren(ziel,hindernisse,true);
        while (kollisionen!=null){
            if(kollisionen.size()>1||!hinderniss_umfahren(kollisionen.get(0),hindernisse))return false;
            kollisionen = fahren(ziel,hindernisse,false);
        }
        return true;
    }

    private ArrayList<Rechteck> an_linke_wand_fahren(Rechteck hinderniss, ArrayList<Rechteck> kontext){
            return fahren(new Vektor(hinderniss.minX()-getRadius(),getPosition().getY()),kontext,false);
        }
    private ArrayList<Rechteck> an_rechte_wand_fahren(Rechteck hinderniss,ArrayList<Rechteck> kontext){
        return fahren(new Vektor(hinderniss.maxX()+getRadius(),getPosition().getY()),kontext,false);
    }
    private ArrayList<Rechteck> an_obere_wand_fahren(Rechteck hinderniss,ArrayList<Rechteck> kontext){
        return fahren(new Vektor(getPosition().getX(),hinderniss.minY()-getRadius()),kontext,false);
    }
    private ArrayList<Rechteck> an_untere_wand_fahren(Rechteck hinderniss,ArrayList<Rechteck> kontext) {
        return fahren(new Vektor(getPosition().getX(),hinderniss.maxY()+getRadius()),kontext,false);
    }





}
